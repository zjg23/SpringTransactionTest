import java.lang.reflect.Type;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import transaction.UserDao;

public class App
{

    public static void main(String[] args)
    {
	ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

	UserDao userDao = (UserDao) ctx.getBean("userDao");
	
	for(int i=0;i<userDao.getClass().getGenericInterfaces().length;i++)
	System.out.println(userDao.getClass().getGenericInterfaces()[i]);
		
	System.out.println(userDao.getClass());

	
	userDao.deleteUser();
    }

}
