package other;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import transaction.impl.UserDaoImpl;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor
{

    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException
    {
	Map bean = beanFactory.getBeansOfType(UserDaoImpl.class);
    }

}
