package transaction.impl;

import org.springframework.jdbc.core.JdbcTemplate;

import transaction.UserDao;

public class UserDaoImpl implements UserDao
{
    private JdbcTemplate jdbcTemplate;
    
    public JdbcTemplate getJdbcTemplate()
    {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void deleteUser(){
	String sql = "delete from UserInfo where id =?";
	
        int id =1;
        jdbcTemplate.update(sql, new Object[]{id});
        
        //simulate exception
        int x = 1/0;
        
        id=2;
        jdbcTemplate.update(sql,new Object[]{id});
        
    }    
}
